package dev.jdittmer.mc.Report;

import dev.jdittmer.mc.Report.MySQL.*;
import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class Report extends JavaPlugin implements Listener {

    private static Plugin plugin;

    public static FileConfiguration config;
    File cfile;

    @Override
    public void onEnable() {
        plugin = this;

        /* config.yml LOADING */
        config = getConfig();
        config.options().copyDefaults(true);
        saveConfig();
        cfile = new File(getDataFolder(), "config.yml");
        System.out.println("[Report]"+SOPANSIColors.GREEN+" config.yml loaded!"+SOPANSIColors.RESET);

        if(MySQL.checkConnection()){
            MySQL.createReportsTable();
            Language.createLangTable();
            Language.createVariables();

            /* TEST CONNECTION TO DATABASE [start] */
            Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
                try {
                    MySQL.conn = DriverManager.getConnection("jdbc:mysql://" + MySQL.dbHost + ":" + MySQL.dbPort + "/" + MySQL.dbName + "?" + "user=" + MySQL.dbUser + "&" + "password=" + MySQL.dbPassword + "&useSSL=" + MySQL.useSSL);
                    System.out.println("[Report]"+SOPANSIColors.GREEN+" Task DB: Test connection established!"+SOPANSIColors.RESET);
                } catch (SQLException e) {
                    System.out.println("[Report]"+SOPANSIColors.RED+" Task DB: Test connection failed!"+SOPANSIColors.RESET);
                    System.out.println("[Report]"+SOPANSIColors.RED+" Task DB: Please edit the config.yml and check your database connection!"+SOPANSIColors.RESET);
                }
            }, 0L, 12000L);
            /* TEST CONNECTION TO DATABASE [end] */

            Bukkit.getServer().getPluginManager().registerEvents(this, this);
            Bukkit.getPluginManager().registerEvents(new GUI(), this);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        boolean pluginIsOnline = true;

        /* CHECK DATABASE CONNECTION */
        if(p.isOp()){
            if(!MySQL.checkConnection()){
                pluginIsOnline = false;
                p.sendMessage(Report.plgChatStart());
                p.sendMessage(Report.pluginTag() + ChatColor.RED + "DB: No connection to database!");
                p.sendMessage(Report.pluginTag() + ChatColor.RED + "DB: Please edit the config.yml and check your database connection!");
                p.sendMessage(Report.plgChatEnd());
                Bukkit.getPluginManager().disablePlugin(this);
            }
        }

        if(pluginIsOnline){
            /* CHECK IF USE THE ACTUALLY PLUGIN VERSION */
            if(p.isOp()){
                CheckVersion.checkVersion(p.getName());
                p.sendMessage("");
            }

            /* CHECK HOW MUCH REPORTS ARE OPEN */
            if(p.hasPermission("report.admin.book")){
                CheckReports.checkNumReports(p.getName());
                p.sendMessage("");
            }
        }
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    public static long timestamp(){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        return timestamp.getTime() / 1000;
    }

    public static String timestampToDate(long timestamp){
        Timestamp timestamp2 = new Timestamp(timestamp * 1000);
        Date date = new Date(timestamp2.getTime());

        if(config.getInt("dateFormat") == 24){
            DateFormat tm = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            return tm.format(date);
        }else if(config.getInt("dateFormat") == 12){
            DateFormat tm = new SimpleDateFormat("MM/dd/yyyy KK:mm:ssa");
            return tm.format(date);
        }

        return "Error config dateFormat";
    }

    public static Date tbExpiresTime(int time, String type){
        long currentTimeMillis = System.currentTimeMillis();
        long banTime;

        switch (type) {
            case "s":
                banTime = time * 1000L;
                return new Date(currentTimeMillis + banTime);
            case "m":
                banTime = 60L * time * 1000;
                return new Date(currentTimeMillis + banTime);
            case "h":
                banTime = 60L * 60 * time * 1000;
                return new Date(currentTimeMillis + banTime);
            case "d":
                banTime = 60L * 60 * 24 * time * 1000;
                return new Date(currentTimeMillis + banTime);
            case "mo":
                banTime = 60L * 60 * 24 * time * 1000;
                banTime = banTime * 30;
                return new Date(currentTimeMillis + banTime);
            case "y":
                banTime = 60L * 60 * 24 * time * 1000;
                banTime = banTime * 365;
                System.out.println("y:" + currentTimeMillis + " + " + banTime);
                return new Date(currentTimeMillis + banTime);
        }

        return null;
    }

    public static String plgChatStart(){
        return ChatColor.GOLD + "--------------------- " + ChatColor.BOLD + "[Report]" + ChatColor.BOLD + " --------------------";
    }

    public static String plgChatEnd(){
        return ChatColor.GOLD + "-------------------------------------------------------";
    }

    public static String noPermissions(){
        return Language.getLang("Miscellaneous.no-permission");
    }

    public static String pluginVersion(){
        return Bukkit.getPluginManager().getPlugin("Report").getDescription().getVersion();
    }

    public static String pluginTag(){
        return ChatColor.GOLD + "[Report] ";
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
        if(cmd.getName().equalsIgnoreCase("report")){
            if(args.length == 0){
                if(sender.hasPermission("report.report")){
                    if(!(sender instanceof Player)){
                        sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.only-players-exec-cmd"));
                        return true;
                    }

                    sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.report-cmd"));
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                    return true;
                }
            }else{
                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("ver")) {
                        if(sender.hasPermission("report.ver")){
                            sender.sendMessage(plgChatStart());
                            sender.sendMessage(ChatColor.RED + "== VERSION");
                            sender.sendMessage(ChatColor.GOLD + "Version: " + ChatColor.WHITE + this.getDescription().getVersion());
                            sender.sendMessage("");
                            sender.sendMessage(ChatColor.GOLD + "Written by: " + ChatColor.WHITE + "Justin Dittmer (JDittmerDEV)");
                            sender.sendMessage(ChatColor.GOLD + "Website: " + ChatColor.WHITE + "https://jdittmer.dev");
                            sender.sendMessage(ChatColor.GOLD + "Spigot: " + ChatColor.WHITE + "https://www.spigotmc.org/members/jnd_3004.282948/");
                            sender.sendMessage(ChatColor.GOLD + "GitLab: " + ChatColor.WHITE + "https://gitlab.com/JDittmerDEV");
                            sender.sendMessage("");
                            sender.sendMessage(ChatColor.GOLD + "Found a bug? Send a mail: " + ChatColor.WHITE + "contact-project+jdittmerdev-minecraft-plugin-report-41588738-issue-@incoming.gitlab.com");
                            sender.sendMessage(plgChatEnd());
                        }else{
                            sender.sendMessage(pluginTag() + noPermissions());
                        }
                        return true;
                    }
                }

                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("help")){
                        if(sender.hasPermission("report.help")){
                            sender.sendMessage(plgChatStart());
                            if(sender.hasPermission("report.report")){
                                sender.sendMessage(Language.getLang("Report.helpList.player-commands"));
                                sender.sendMessage(Language.getLang("Report.helpList.open-the-help"));
                                sender.sendMessage(Language.getLang("Report.helpList.show-the-version"));
                                sender.sendMessage(Language.getLang("Report.helpList.report-a-player"));
                            }
                            if(sender.hasPermission("report.admin")){
                                sender.sendMessage("");
                                sender.sendMessage(Language.getLang("Report.helpList.admin-commands"));
                                sender.sendMessage(Language.getLang("Report.helpList.ban-a-player"));
                                sender.sendMessage(Language.getLang("Report.helpList.tempban-a-player"));
                                sender.sendMessage(Language.getLang("Report.helpList.unban-a-player"));
                                sender.sendMessage(Language.getLang("Report.helpList.kick-a-player"));
                                sender.sendMessage("");
                                sender.sendMessage(Language.getLang("Report.helpList.open-report-book"));
                                sender.sendMessage(Language.getLang("Report.helpList.accept-report"));
                                sender.sendMessage(Language.getLang("Report.helpList.delete-report"));
                                sender.sendMessage("");
                                sender.sendMessage(Language.getLang("Report.helpList.update-lang-db"));
                                sender.sendMessage(Language.getLang("Report.helpList.reload-config"));
                            }
                            sender.sendMessage(plgChatEnd());
                            return true;
                        }
                    }
                }

                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("reload")) {
                        if(sender.hasPermission("report.admin.reload")){
                            config = YamlConfiguration.loadConfiguration(cfile);
                            sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.reload-message"));

                            /* TEST CONNECTION TO DATABASE [start] */
                            try {
                                MySQL.conn = DriverManager.getConnection("jdbc:mysql://" + MySQL.dbHost + ":" + MySQL.dbPort + "/" + MySQL.dbName + "?" + "user=" + MySQL.dbUser + "&" + "password=" + MySQL.dbPassword + "&useSSL=" + MySQL.useSSL);
                                System.out.println("[Report]"+SOPANSIColors.GREEN+" DB: Test connection established!"+SOPANSIColors.RESET);
                                sender.sendMessage(pluginTag() + ChatColor.GREEN + "Database: Test connection established!");
                            } catch (SQLException e) {
                                System.out.println("[Report]"+SOPANSIColors.RED+" DB: Test connection failed!"+SOPANSIColors.RESET);
                                System.out.println("[Report]"+SOPANSIColors.RED+" DB: Please edit the config.yml and check your database connection!"+SOPANSIColors.RESET);
                                sender.sendMessage(pluginTag() + ChatColor.RED + "Database: Test connection failed!");
                                sender.sendMessage(pluginTag() + ChatColor.RED + "Database: Please edit the config.yml and check your database connection!");
                            }
                            /* TEST CONNECTION TO DATABASE [end] */

                        }else{
                            sender.sendMessage(pluginTag() + noPermissions());
                        }
                        return true;
                    }
                }

                /* REPORT PLAYER */
                if(args.length >= 2){
                    if(sender.hasPermission("report.report")){
                        if(!(sender instanceof Player)){
                            sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.only-players-exec-cmd"));
                            return true;
                        }

                        Player p = (Player) sender;

                        long report_date = timestamp();
                        String from_user = p.getName();
                        String report_user = args[0];

                        String report_reason = "";
                        for (int i = 1; i < args.length;) {
                            report_reason += args[i] + " ";
                            i++;
                        }
                        report_reason = report_reason.trim();

                        SaveReport.addUserReport(sender, report_date, from_user, report_user, report_reason);
                    }else{
                        sender.sendMessage(pluginTag() + noPermissions());
                    }
                    return true;
                }

                /* OPEN REPORT BOOK */
                if(args.length == 1){
                    if(args[0].equalsIgnoreCase("book")){
                        if(!(sender instanceof Player)){
                            sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.only-players-exec-cmd"));
                            return true;
                        }

                        if(sender.hasPermission("report.admin.book")){
                            Player p = (Player) sender;
                            GUI.openGUI(p, 1, 0, 53);
                        }else{
                            sender.sendMessage(pluginTag() + noPermissions());
                            return true;
                        }
                    }
                }
            }
        }

        /* UPDATE LANGUAGE DB */
        if(cmd.getName().equalsIgnoreCase("rupdate")){
            if(args.length == 1){
                if(args[0].equalsIgnoreCase("lang")){
                    if(sender.hasPermission("report.admin.updatedb")){
                        Language.updateLang(sender);
                    }else{
                        sender.sendMessage(pluginTag() + noPermissions());
                    }
                    return true;
                }
            }
        }

        /* ACCEPT PLAYER REPORT */
        if(cmd.getName().equalsIgnoreCase("raccept")){
            if(!(sender instanceof Player)){
                sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.only-players-exec-cmd"));
                return true;
            }

            if(args.length == 1){
                if(sender.hasPermission("report.admin.report.accept")){
                    AcceptReport.acceptUserReport(sender, args[0], sender.getName());
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                }
                return true;
            }
        }

        /* DELETE PLAYER REPORT */
        if(cmd.getName().equalsIgnoreCase("rdelete")){
            if(!(sender instanceof Player)){
                sender.sendMessage(pluginTag() + Language.getLang("Report.helpList.only-players-exec-cmd"));
                return true;
            }

            if(args.length == 1){
                if(sender.hasPermission("report.admin.report.delete")){
                    DeleteReport.deleteUserReport(sender, args[0]);
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                }
                return true;
            }
        }

        /* BAN PLAYER */
        if(cmd.getName().equalsIgnoreCase("b")){
            if(args.length < 2){
                if(sender.hasPermission("report.admin.ban")){
                    sender.sendMessage(pluginTag() + Language.getLang("BanCMD.cmd"));
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                    return true;
                }
            }else{
                if(args.length >= 2){
                    if(sender.hasPermission("report.admin.ban")){

                        String ban_player = args[0];
                        String ban_reason = "";
                        for (int i = 1; i < args.length;) {
                            ban_reason += args[i] + " ";
                            i++;
                        }
                        ban_reason = ban_reason.trim();

                        Bukkit.getBanList(BanList.Type.NAME).addBan(ban_player, ban_reason, null, sender.getName());

                        Player player = Bukkit.getPlayer(ban_player);
                        if(player != null){
                            player.kickPlayer(Language.getLang("BanCMD.screen-was-banned") + ChatColor.BOLD + ban_reason);
                        }

                        sender.sendMessage(pluginTag() + Language.getLang("BanCMD.the-player") + ban_player + Language.getLang("BanCMD.was-banned"));
                        System.out.println("[Report]"+SOPANSIColors.RED+" "+ ban_player +" was banned by "+ sender.getName() +SOPANSIColors.RESET);
                    }else{
                        sender.sendMessage(pluginTag() + noPermissions());
                    }
                }else{
                    sender.sendMessage(pluginTag() + Language.getLang("BanCMD.cmd"));
                }
                return true;
            }
        }

        /* TEMPBAN PLAYER */
        if(cmd.getName().equalsIgnoreCase("tb")){
            if(args.length < 3){
                if(sender.hasPermission("report.admin.tempban")){
                    sender.sendMessage(pluginTag() + Language.getLang("TempbanCMD.cmd"));
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                    return true;
                }
            }else{
                if(args.length >= 3){
                    if(sender.hasPermission("report.admin.tempban")){

                        String tempban_player = args[0];
                        String[] tempban_time = args[1].split("(?<=[0-9])(?=[a-zA-Z])");
                        String tempban_reason = "";
                        for (int i = 2; i < args.length;) {
                            tempban_reason += args[i] + " ";
                            i++;
                        }
                        tempban_reason = tempban_reason.trim();

                        if(Integer.parseInt(tempban_time[0]) > 0){
                            Bukkit.getBanList(BanList.Type.NAME).addBan(tempban_player, tempban_reason, tbExpiresTime(Integer.parseInt(tempban_time[0]), tempban_time[1]), sender.getName());

                            Player player = Bukkit.getPlayer(tempban_player);
                            if(player != null){
                                player.kickPlayer(Language.getLang("TempbanCMD.screen-was-banned") + ChatColor.BOLD + tempban_reason + ChatColor.RESET + Language.getLang("TempbanCMD.screen-until") + ChatColor.BOLD + tbExpiresTime(Integer.parseInt(tempban_time[0]), tempban_time[1]));
                            }

                            sender.sendMessage(pluginTag() + Language.getLang("TempbanCMD.the-player") + tempban_player + Language.getLang("TempbanCMD.was-banned"));
                            System.out.println("[Report]"+SOPANSIColors.RED+" "+ tempban_player +" was temporary banned by "+ sender.getName() +SOPANSIColors.RESET);
                            return true;
                        }
                    }else{
                        sender.sendMessage(pluginTag() + noPermissions());
                        return true;
                    }
                }else{
                    sender.sendMessage(pluginTag() + Language.getLang("TempbanCMD.cmd"));
                    return true;
                }
            }
        }

        /* UNBAN PLAYER */
        if(cmd.getName().equalsIgnoreCase("ub")){
            if(args.length < 1){
                if(sender.hasPermission("report.admin.unban")){
                    sender.sendMessage(pluginTag() + Language.getLang("UnbanCMD.cmd"));
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                    return true;
                }
            }else{
                if(args.length == 1){
                    if(sender.hasPermission("report.admin.unban")){
                        String unban_player = args[0];

                        Bukkit.getBanList(BanList.Type.NAME).pardon(unban_player);
                        sender.sendMessage(pluginTag() + Language.getLang("UnbanCMD.the-player") + unban_player + Language.getLang("UnbanCMD.was-unban"));
                        System.out.println("[Report]"+SOPANSIColors.GREEN+" "+ unban_player +" was unbanned by "+ sender.getName() +SOPANSIColors.RESET);
                    }else{
                        sender.sendMessage(pluginTag() + noPermissions());
                    }
                }else{
                    sender.sendMessage(pluginTag() + Language.getLang("UnbanCMD.cmd"));
                }
                return true;
            }
        }

        /* KICK PLAYER */
        if(cmd.getName().equalsIgnoreCase("k")){
            if(args.length < 2){
                if(sender.hasPermission("report.admin.kick")){
                    sender.sendMessage(pluginTag() + Language.getLang("KickCMD.cmd"));
                }else{
                    sender.sendMessage(pluginTag() + noPermissions());
                    return true;
                }
            }else{
                if(args.length >= 2){
                    if(sender.hasPermission("report.admin.kick")){
                        Player kick_player = Bukkit.getPlayer(args[0]);

                        if(kick_player != null && kick_player.isOnline()){
                            String kick_reason = "";
                            for (int i = 1; i < args.length;) {
                                kick_reason += args[i] + " ";
                                i++;
                            }
                            kick_reason = kick_reason.trim();

                            kick_player.kickPlayer(Language.getLang("KickCMD.screen-was-kicked") + ChatColor.BOLD + kick_reason);
                            sender.sendMessage(pluginTag() + Language.getLang("KickCMD.the-player") + args[0] + Language.getLang("KickCMD.was-kicked"));
                            System.out.println("[Report]"+SOPANSIColors.RED+" "+ kick_player +" was kicked by "+ sender.getName() +SOPANSIColors.RESET);
                            return true;
                        }else if(kick_player == null){
                            sender.sendMessage(pluginTag() + Language.getLang("KickCMD.the-player2") + args[0] + Language.getLang("KickCMD.is-not-online"));
                            return true;
                        }
                    }else{
                        sender.sendMessage(pluginTag() + noPermissions());
                        return true;
                    }
                }else{
                    sender.sendMessage(pluginTag() + Language.getLang("KickCMD.cmd"));
                    return true;
                }
            }
        }

        return true;
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
