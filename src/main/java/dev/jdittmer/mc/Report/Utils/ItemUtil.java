package dev.jdittmer.mc.Report.Utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

public class ItemUtil {
    public static ItemStack create(String displayName, Material material, String lore1, String lore2, String lore3, String lore4, String lore5, String lore6, String lore7, int amount) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta meta = item.getItemMeta();

        if (displayName != null) {
            meta.setDisplayName(displayName);
        }

        ArrayList<String> lore = new ArrayList<String>();

        if (lore1 != null){
            lore.add(lore1);
        }
        if (lore2 != null){
            lore.add(lore2);
        }
        if (lore3 != null){
            lore.add(lore3);
        }
        if (lore4 != null){
            lore.add(lore4);
        }
        if (lore5 != null){
            lore.add(lore5);
        }
        if (lore6 != null){
            lore.add(lore6);
        }
        if (lore7 != null){
            lore.add(lore7);
        }
        meta.setLore(lore);
        item.setItemMeta(meta);

        return item;
    }
}
