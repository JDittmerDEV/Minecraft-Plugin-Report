package dev.jdittmer.mc.Report.MySQL;

import dev.jdittmer.mc.Report.Report;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SaveReport {
    public static boolean addUserReport(CommandSender sender, long date, String from_user, String report_user, String report_reason)
    {
        Player report_user2 = Bukkit.getPlayer(report_user);

        if(report_user2 == null){
            // CHECK IS REPORTED USER ONLINE
            sender.sendMessage(Report.pluginTag() + Language.getLang("SaveReport.the-player") + "\"" + report_user + "\"" + Language.getLang("SaveReport.not-online"));
            return false;
        }

        if(sender.getName().equals(report_user2.getName())){
            // CHECK IS REPORTED USER YOURSELF
            sender.sendMessage(Report.pluginTag() + Language.getLang("SaveReport.cant-report-yourself"));
            return false;
        }

        String report_user_world = report_user2.getLocation().getWorld().toString();
        int report_user_x = report_user2.getLocation().getBlockX();
        int report_user_y = report_user2.getLocation().getBlockY();
        int report_user_z = report_user2.getLocation().getBlockZ();
        float report_user_yaw = report_user2.getLocation().getYaw();
        float report_user_pitch = report_user2.getLocation().getPitch();
        int agent = 0;
        String agent_name = "";

        MySQL.conn = MySQL.getInstance();
        if(MySQL.conn != null) {
            Statement query;
            try {
                query = MySQL.conn.createStatement();

                /* MYSQL - INSERT PLAYER REPORT [start] */
                report_user_world = report_user_world.replace("CraftWorld{name=", "").replace("}", "").toString();

                String insertReport = "INSERT INTO report_reports(date, report_id, from_user, report_user, report_user_world, report_user_x, report_user_y, report_user_z, report_user_yaw, report_user_pitch, report_reason, agent, agent_name) " +
                        "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                PreparedStatement preparedStatement = MySQL.conn.prepareStatement(insertReport);
                preparedStatement.setLong(1, date);
                for ( int i = 0; i <= 1000; i ++ ) {
                    String sql2 = "SELECT COUNT(*) AS report_id FROM report_reports WHERE report_id = " + i;
                    ResultSet result2 = query.executeQuery(sql2);
                    result2.next();
                    int rows = result2.getInt("report_id");

                    if(i < 1000){
                        if(rows == 0){
                            preparedStatement.setInt(2, i);
                            break;
                        }
                    }else if(i == 1000){
                        sender.sendMessage(Report.plgChatStart());
                        sender.sendMessage(Language.getLang("SaveReport.too-many-reports-db"));
                        sender.sendMessage(Language.getLang("SaveReport.try-again-later"));
                        sender.sendMessage(Report.plgChatEnd());
                        return false;
                    }
                }
                preparedStatement.setString(3, from_user);
                preparedStatement.setString(4, report_user);
                preparedStatement.setString(5, report_user_world);
                preparedStatement.setInt(6, report_user_x);
                preparedStatement.setInt(7, report_user_y);
                preparedStatement.setInt(8, report_user_z);
                preparedStatement.setFloat(9, report_user_yaw);
                preparedStatement.setFloat(10, report_user_pitch);
                preparedStatement.setString(11, report_reason);
                preparedStatement.setInt(12, agent);
                preparedStatement.setString(13, agent_name);

                if(!report_user2.isOp()){
                    if(CheckSpamReport.checkSpamReport(sender, from_user, report_user) == true){
                        preparedStatement.executeUpdate();
                        sender.sendMessage(Report.pluginTag() + Language.getLang("SaveReport.report-successfully-sended"));
                        Bukkit.broadcast(Report.pluginTag() + ChatColor.RED + Language.getLang("SaveReport.the-player") + "\"" + ChatColor.DARK_RED + report_user + ChatColor.RED + "\"" + Language.getLang("SaveReport.has-just-been-reported"), "report.admin.book");
                    }else if(CheckSpamReport.checkSpamReport(sender, from_user, report_user) == false){
                        sender.sendMessage(Report.pluginTag() + Language.getLang("SaveReport.report-every-30minutes"));
                    }
                }else{
                    sender.sendMessage(Report.pluginTag() + Language.getLang("SaveReport.op-cant-be-reported"));
                }
                /* MYSQL - INSERT PLAYER REPORT [end] */
            } catch (SQLException e) {
                e.printStackTrace();
                sender.sendMessage(Report.pluginTag() + ChatColor.RED + "Error: \"MySQL.SaveReport.java\" - Please contact the server admin!");
                sender.sendMessage(Report.pluginTag() + ChatColor.RED + "Error: " + e.getMessage());
            }
        }

        return true;
    }
}
