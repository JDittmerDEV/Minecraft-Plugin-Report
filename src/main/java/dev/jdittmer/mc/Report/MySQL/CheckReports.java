package dev.jdittmer.mc.Report.MySQL;

import dev.jdittmer.mc.Report.Report;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckReports {
    public static boolean checkNumReports(String user_name){
        MySQL.conn = MySQL.getInstance();
        if(MySQL.conn != null) {
            try {
                Player player = Bukkit.getPlayer(user_name);

                PreparedStatement query = MySQL.conn.prepareStatement("SELECT COUNT(*) FROM report_reports WHERE agent = 0");
                ResultSet result = query.executeQuery();

                result.next();
                int rows = result.getInt("COUNT(*)");

                if(rows == 1){
                    player.sendMessage(Report.plgChatStart());
                    player.sendMessage(Report.pluginTag() + Language.getLang("CheckReports.1report-are-open"));
                    player.sendMessage(Report.pluginTag() + Language.getLang("CheckReports.use-book-see-reports"));
                    player.sendMessage(Report.plgChatEnd());
                }else if(rows > 1){
                    player.sendMessage(Report.plgChatStart());
                    player.sendMessage(Report.pluginTag() + Language.getLang("CheckReports.there-are-currently") + rows + Language.getLang("CheckReports.reports-open"));
                    player.sendMessage(Report.pluginTag() + Language.getLang("CheckReports.use-book-see-reports"));
                    player.sendMessage(Report.plgChatEnd());
                }else if(rows == 0){
                    player.sendMessage(Report.pluginTag() + Language.getLang("CheckReports.no-reports-available"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return true;
    }
}
