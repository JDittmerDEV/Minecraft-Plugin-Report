package dev.jdittmer.mc.Report.MySQL;

import dev.jdittmer.mc.Report.Report;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DeleteReport {
    public static boolean deleteUserReport(CommandSender sender, String reportID) {
        MySQL.conn = MySQL.getInstance();

        if(MySQL.conn != null) {
            try {
                PreparedStatement query = MySQL.conn.prepareStatement("SELECT * FROM report_reports WHERE report_id = ?");
                query.setString(1, reportID);
                ResultSet result = query.executeQuery();

                if(result.next()){
                    PreparedStatement sql = MySQL.conn.prepareStatement("DELETE FROM report_reports WHERE report_id = ?");
                    sql.setString(1, reportID);
                    sql.executeUpdate();

                    sender.sendMessage(Report.pluginTag() + Language.getLang("DeleteReport.the-report-id") + reportID + Language.getLang("DeleteReport.was-removed"));
                }else{
                    sender.sendMessage(Report.pluginTag() + Language.getLang("DeleteReport.the-report-id") + reportID + Language.getLang("DeleteReport.was-not-found-in-db"));
                    return false;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                sender.sendMessage(Report.pluginTag() + ChatColor.RED + "Error: \"MySQL.DeletePlayerReport.java\" - Please contact the server admin!");
                sender.sendMessage(Report.pluginTag() + ChatColor.RED + "Error: " + e.getMessage());
            }
        }
        return true;
    }
}
