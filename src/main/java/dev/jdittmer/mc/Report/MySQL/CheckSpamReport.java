package dev.jdittmer.mc.Report.MySQL;

import dev.jdittmer.mc.Report.Report;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckSpamReport {
    public static boolean checkSpamReport(CommandSender sender, String from_user, String report_user){
        MySQL.conn = MySQL.getInstance();
        if(MySQL.conn != null) {
            try {
                Player p_report_user = Bukkit.getPlayer(report_user);

                PreparedStatement checkDoubleReport = MySQL.conn.prepareStatement("SELECT * FROM report_reports WHERE from_user = ? ORDER BY `date` DESC LIMIT 1");
                checkDoubleReport.setString(1, from_user);
                ResultSet checkDoubleReportResult = checkDoubleReport.executeQuery();

                while (checkDoubleReportResult.next()) {
                    if(p_report_user.getName().equals(checkDoubleReportResult.getString("report_user"))){
                        if((Report.timestamp() - checkDoubleReportResult.getLong("date")) > 1800){
                            return true;
                        }else{
                            return false;
                        }
                    }else{
                        return true;
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
                sender.sendMessage(Report.pluginTag() + ChatColor.RED + "Error: \"MySQL.CheckSpamReport.java\" - Please contact the server admin!");
                sender.sendMessage(Report.pluginTag() + ChatColor.RED + "Error: " + e.getMessage());
            }
        }
        return true;
    }
}
