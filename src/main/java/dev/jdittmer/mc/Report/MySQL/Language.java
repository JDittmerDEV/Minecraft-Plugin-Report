package dev.jdittmer.mc.Report.MySQL;

import dev.jdittmer.mc.Report.ConvChatColors;
import dev.jdittmer.mc.Report.Report;
import dev.jdittmer.mc.Report.SOPANSIColors;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Language {
    public static void createLangTable(){
        MySQL.conn = MySQL.getInstance();

        if(MySQL.conn != null) {
            try {
                PreparedStatement createTable = MySQL.conn.prepareStatement("CREATE TABLE IF NOT EXISTS report_lang (id INT(11) NOT NULL AUTO_INCREMENT, var VARCHAR(500) NOT NULL, de VARCHAR(500) NOT NULL, en VARCHAR(500) NOT NULL, UNIQUE KEY id (id)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
                createTable.executeUpdate();
                System.out.println("[Report]"+ SOPANSIColors.GREEN+" DB: report_lang table was created."+SOPANSIColors.RESET);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void createVariables(){
        MySQL.conn = MySQL.getInstance();
        if(MySQL.conn != null) {
            try {
                System.out.println("[Report]"+SOPANSIColors.YELLOW+" DB: report_lang variables will now created."+SOPANSIColors.RESET);
                PreparedStatement truncateTable = MySQL.conn.prepareStatement("TRUNCATE TABLE report_lang;");
                truncateTable.executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Miscellaneous.no-permission', '&cDu hast nicht gengend Rechte um diesen Befehl auszufhren!', '&cYou have not enough permission for this command!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.only-players-exec-cmd', '&cNur Spieler knnen diesen Befehl ausfhren!', '&cOnly players can execute this command!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.report-cmd', '&c/report <spieler> <grund>', '&c/report <player> <reason>')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.player-commands', '&c== SPIELER BEFEHLE', '&c== PLAYER COMMANDS')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.open-the-help', '&6/report help&f: ffne die Hilfe/Befehlsliste', '&6/report help&f: Open the help/commands list')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.show-the-version', '&6/report ver&f: Zeigt dir die aktuelle Version an', '&6/report ver&f: Shows you the actually version')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.report-a-player', '&6/report <spieler> <grund>&f: Melde einen Spieler', '&6/report <player> <reason>&f: Report a player')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.admin-commands', '&c== ADMIN BEFEHLE', '&c== ADMIN COMMANDS')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.ban-a-player', '&6/b <spieler> <grund>&f: Banne einen Spieler', '&6/b <player> <reason>&f: Ban a player')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.tempban-a-player', '&6/tb <spieler> <[zeit]s|m|h|d|mo|y> <grund>&f: Banne einen Spieler temporr', '&6/tb <player> <[time]s|m|h|d|mo|y> <reason>&f: Ban a player temporary')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.unban-a-player', '&6/ub <spieler>&f: Entbanne einen Spieler', '&6/ub <player>&f: Unban a player')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.kick-a-player', '&6/k <spieler> <grund>&f: Kick einen Spieler', '&6/k <player> <reason>&f: Kick a player')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.open-report-book', '&6/report book&f: ffne das Report Buch', '&6/report book&f: Open the report book')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.accept-report', '&6/raccept <reportID>&f: Akzeptiere einen Spieler Report', '&6/raccept <reportID>&f: Accept a player report')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.delete-report', '&6/rdelete <reportID>&f: Lsche einen Spieler Report', '&6/rdelete <reportID>&f: Delete a player report')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.update-lang-db', '&6/rupdate lang&f: Aktualisiere die Sprachdatenbank', '&6/rupdate lang&f: Update the language database')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.reload-config', '&6/report reload&f: Konfig neu laden', '&6/report reload&f: Reload the config')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Report.helpList.reload-message', '&aKonfig wurde neu geladen!', '&aConfig was reloaded!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('BanCMD.cmd', '&c/b <spieler> <grund>', '&c/b <player> <reason>')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('BanCMD.the-player', '&aDer Spieler &c', '&aThe player &c')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('BanCMD.was-banned', ' &awurde permanent gebannt!', ' &awas permanently banned!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('BanCMD.screen-was-banned', ' &cDu wurdest permanent gebannt wegen&f ', ' &cYou have been permanently banned for&f ')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('TempbanCMD.cmd', '&c/tb <spieler> <[zeit]s|m|h|d|mo|y> <grund>', '&c/tb <player> <[time]s|m|h|d|mo|y> <reason>')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('TempbanCMD.the-player', '&aDer Spieler &c', '&aThe player &c')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('TempbanCMD.was-banned', ' &awurde temporär gebannt!', ' &awas temporary banned!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('TempbanCMD.screen-was-banned', ' &cDu wurdest temporär gebannt wegen&f ', ' &cYou have been temporarily banned for&f ')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('TempbanCMD.screen-until', ' &cbis zum&f ', ' &cuntil&f ')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('UnbanCMD.cmd', '&c/ub <spieler>', '&c/ub <player>')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('UnbanCMD.the-player', '&aDer Spieler &c', '&aThe player &c')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('UnbanCMD.was-unban', ' &awurde entbannt!', ' &awas unban!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('KickCMD.cmd', '&c/k <spieler> <grund>', '&c/k <player> <reason>')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('KickCMD.the-player', '&aDer Spieler &c', '&aThe player &c')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('KickCMD.was-kicked', ' &awurde gekickt!', ' &awas kicked!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('KickCMD.the-player2', '&cDer Spieler \"', '&cThe player \"')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('KickCMD.screen-was-kicked', '&cDu wurdest gekickt wegen&f ', '&cYou got kicked for&f ')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('KickCMD.is-not-online', '\" ist nicht Online!', '\" is not online!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('AcceptReport.accept-report', '&aDu hast den Report akzeptiert!', '&aYou have now accept the report!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('AcceptReport.no-report', '&cEs gibt keinen Report mit dieser ID!', '&cThere is no report with this ID!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('DeleteReport.the-report-id', '&cDie Report ID: ', '&cThe report ID: ')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('DeleteReport.was-removed', ' wurde gelscht!', ' was removed!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('DeleteReport.was-not-found-in-db', ' wurde in der Datenbank nicht gefunden!', ' was not found in the database!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckReports.1report-are-open', '&cEs ist derzeit 1 Report offen!', '&cThere is currently 1 report open!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckReports.there-are-currently', '&cEs sind derzeit ', '&cThere are currently ')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckReports.reports-open', ' Reports offen!', ' reports open!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckReports.use-book-see-reports', '&7Benutze &6/report book &7um die Reports einsehen zu knnen.', '&7Use &6/report book &7to view the reports.')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckReports.no-reports-available', '&aZurzeit sind keine Reports offen!', '&aAt the moment there are no reports available!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckVersion.you-use-a-old-version', '&cDu benutzt eine veraltete Version von \"&4Report&c\"!', '&cYou are using an outdated version of \"&4Report&c\"!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckVersion.please-dl-new-version', '&cBitte downloade die &2neue Version &a(', '&cPlease download the &2new version &a(')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('CheckVersion.and-reload-server', ')&c und reloade deinen Server.', ')&c and reload your server.')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.all-reports', 'Alle Meldungen', 'All reports')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.site', 'Seite: ', 'Site: ')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.forward', '&aWeiter -->', '&aForward -->')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.backward', '&c<-- Zurck', '&c<-- Backward')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.report-date', 'Meldedatum: &f', 'Report date: &f')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.reported-user', 'Gemeldeter User: &c', 'Reported user: &c')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.reported-by', 'Gemeldet von: &a', 'Reported by: &a')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.reason', 'Grund: &f', 'Reason: &f')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.agent', 'Betreuer:', 'Agent:')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.agent-yes', ' &aJa', ' &aYes')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.agent-no', ' &cNein', ' &cNo')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.agent-name', 'Betreuer Name: &a', 'Agent name: &a')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('GUI.or', ' &fODER&c ', ' &fOR&c ')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.the-player', '&cDer Spieler ', '&cThe player ')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.not-online', ' ist nicht Online und kann nicht gemeldet werden!', ' is not online and cannot be reported!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.cant-report-yourself', '&cDu kannst dich selber nicht melden!', '&cYou cannot report yourself!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.too-many-reports-db', '&cZurzeit sind zu viele Meldungen in der Datenbank.', '&cThere are currently too many reports in the database.')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.try-again-later', '&cBitte versuche es spter erneut oder kontaktiere ein Teammitglied auf dem Server!', '&cPlease try again later or contact a team member on the server!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.report-successfully-sended', '&aDein Report wurde erfolgreich versendet!', '&aYour report was successfully sended!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.has-just-been-reported', ' wurde eben gemeldet!', ' has just been reported!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.report-every-30minutes', '&cDu kannst nur alle 30 Minuten die gleiche Person melden!', '&cYou can only report the same person every 30 minutes!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('SaveReport.op-cant-be-reported', '&cOP Spieler knnen nicht gemeldet werden!', '&cOP players can not be reported!')").executeUpdate();

                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Language.table-was-recreated', '&aDie Sprachdatenbank wurde neu erstellt!', '&aThe language database was recreated!')").executeUpdate();
                MySQL.conn.prepareStatement("INSERT INTO report_lang(var, de, en) VALUES('Language.table-recreated-error', '&cEs gab einen Fehler beim neu erstellen der Sprachdatenbank!', '&cThere was an error when recreating the language database!')").executeUpdate();

                System.out.println("[Report]"+SOPANSIColors.GREEN+" DB: report_lang variables was created."+SOPANSIColors.RESET);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void updateLang(CommandSender sender){
        MySQL.conn = MySQL.getInstance();

        if(MySQL.conn != null) {
            try {
                PreparedStatement dropTable = MySQL.conn.prepareStatement("DROP TABLE IF EXISTS report_lang;");
                dropTable.executeUpdate();

                sender.sendMessage(Report.pluginTag() + ChatColor.GOLD + "The language database will be recreated!");

                createLangTable();
                createVariables();

                System.out.println("[Report]"+SOPANSIColors.GREEN+" DB: report_lang table was recreated."+SOPANSIColors.RESET);
                sender.sendMessage(Report.pluginTag() + getLang("Language.table-was-recreated"));
            } catch (SQLException e) {
                e.printStackTrace();
                sender.sendMessage(Report.pluginTag() + getLang("Language.table-recreated-error"));
            }
        }
    }

    public static String getLang(String var){
        MySQL.conn = MySQL.getInstance();
        if(MySQL.conn != null) {
            Statement query;
            try {
                query = MySQL.conn.createStatement();

                String getVariable = "SELECT * FROM report_lang WHERE var = '" + var + "'";
                ResultSet getVariableResult = query.executeQuery(getVariable);

                while(getVariableResult.next()){
                    return ConvChatColors.ConvColor(getVariableResult.getString(Report.config.getString("language")));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return var;
    }
}