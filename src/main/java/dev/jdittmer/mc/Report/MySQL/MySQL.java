package dev.jdittmer.mc.Report.MySQL;

import dev.jdittmer.mc.Report.Report;
import dev.jdittmer.mc.Report.SOPANSIColors;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQL {
    public static Connection conn = null;

    public static String dbHost = Report.config.getString("conn-db.host");
    public static String dbPort = Report.config.getString("conn-db.port");
    public static String dbName = Report.config.getString("conn-db.name");
    public static String dbUser = Report.config.getString("conn-db.user");
    public static String dbPassword = Report.config.getString("conn-db.pass");
    public static String useSSL = Report.config.getString("conn-db.useSSL");

    public MySQL() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            conn = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?" + "user=" + dbUser + "&" + "password=" + dbPassword + "&useSSL=" + useSSL);
            System.out.println("[Report]"+ SOPANSIColors.GREEN+" DB: Connection established! Brilliant \\o/"+SOPANSIColors.RESET);
        } catch (ClassNotFoundException e) {
            //
        } catch (SQLException e) {
            //
        }
    }

    public static Connection getInstance() {
        if(conn == null)
            new MySQL();
        return conn;
    }

    public static boolean checkConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?" + "user=" + dbUser + "&" + "password=" + dbPassword + "&useSSL=" + useSSL);
            System.out.println("[Report]"+SOPANSIColors.GREEN+" DB: Connection established! Brilliant \\o/"+SOPANSIColors.RESET);
            return true;
        } catch (ClassNotFoundException e) {
            Bukkit.getPluginManager().disablePlugin(Report.getPlugin());
            System.out.println("[Report]"+SOPANSIColors.RED+" The plugin was disabled!"+SOPANSIColors.RESET);
            System.out.println("[Report] =======================================");
            System.out.println("[Report]"+SOPANSIColors.RED+" DB: Driver not found!"+SOPANSIColors.RESET);
        } catch (SQLException e) {
            Bukkit.getPluginManager().disablePlugin(Report.getPlugin());
            System.out.println("[Report]"+SOPANSIColors.RED+" The plugin was disabled!"+SOPANSIColors.RESET);
            System.out.println("[Report] =======================================");
            System.out.println("[Report]"+SOPANSIColors.RED+" DB: No connection to database!"+SOPANSIColors.RESET);
            System.out.println("[Report]"+SOPANSIColors.RED+" DB: Please edit the config.yml and check your database connection!"+SOPANSIColors.RESET);
            return false;
        }
        return false;
    }

    public static void createReportsTable() {
        conn = getInstance();

        if(conn != null) {
            try {
                PreparedStatement preparedStatement = conn.prepareStatement("CREATE TABLE IF NOT EXISTS report_reports (id INT(11) NOT NULL AUTO_INCREMENT, report_id INT(11) NOT NULL, date INT(11) NOT NULL, from_user VARCHAR(30) NOT NULL, report_user VARCHAR(30) NOT NULL, report_user_world VARCHAR(30) NOT NULL, report_user_x VARCHAR(20) NOT NULL, report_user_y VARCHAR(20) NOT NULL, report_user_z VARCHAR(20) NOT NULL, report_user_yaw VARCHAR(20) NOT NULL, report_user_pitch VARCHAR(20) NOT NULL, report_reason TEXT NOT NULL, agent INT(11) NOT NULL, agent_name VARCHAR(30) NOT NULL, UNIQUE KEY id (id)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
                preparedStatement.executeUpdate();
                System.out.println("[Report]"+SOPANSIColors.GREEN+" DB: report_reports table was found."+SOPANSIColors.RESET);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}

