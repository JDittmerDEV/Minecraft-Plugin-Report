package dev.jdittmer.mc.Report;

import dev.jdittmer.mc.Report.MySQL.Language;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

public class CheckVersion {
    public static void checkVersion(String user_name){

        Player player = Bukkit.getPlayer(user_name);

        try {
            URLConnection connection = new URL("https://gitlab.com/JDittmerDEV/Minecraft-Plugin-Report/-/raw/master/versions/version.txt").openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.connect();

            BufferedReader r = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));

            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                sb.append(line);
            }
            String value = sb.toString();

            if(!value.equalsIgnoreCase(Report.pluginVersion())){
                player.sendMessage(Report.plgChatStart());
                player.sendMessage(Report.pluginTag() + Language.getLang("CheckVersion.you-use-a-old-version"));
                player.sendMessage(Report.pluginTag() + Language.getLang("CheckVersion.please-dl-new-version") + value + Language.getLang("CheckVersion.and-reload-server"));
                player.sendMessage(Report.pluginTag() + ChatColor.GRAY + "https://www.spigotmc.org/resources/report.54161/history");
                player.sendMessage(Report.plgChatEnd());
            }
        }catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}

